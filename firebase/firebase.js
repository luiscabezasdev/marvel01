import firebase from 'firebase/app';
import 'firebase/auth';

  if (!firebase.apps.length) {
    firebase.initializeApp({
      apiKey: "AIzaSyBh81pBaSnq8G-EoL_3W8ihuIsnSSAfIHg",
      authDomain: "nextjs-with-marvel.firebaseapp.com",
      projectId: "nextjs-with-marvel",
    });
  }

export default firebase;