import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import ButtonSecundary from './ButtonSecundary';

const Start = () => {

  return (
    <>
      <div className="start">
        <Image
          className="img"
          src="/Logo.png"
          width={356}
          height={126.51}
        />
        <p>Descubre tus personajes favoritos, y mira en <br/>que cómics tienen sus grandes historias. </p>
        <p>Ya tienes una cuenta con nosotros, <br/>que esperas ingresa.</p>

          <Link href="/register">
            <a>
              <ButtonSecundary>
                Iniciar Sesión
              </ButtonSecundary>
            </a>
          </Link>

      </div>
    </>
  )
}

export default Start;