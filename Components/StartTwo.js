import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import ButtonSecundary from '../Components/ButtonSecundary';

const StartTwo = () => {
  return (
    <>
      <div className="start">
        <Image
          className="img"
          src="/Logo.png"
          width={356}
          height={126.51}
        />
        <p>Descubre tus personajes favoritos, y mira en <br/>que cómics tienen sus grandes historias. </p>
          <p>No tienes una cuenta con nosotros, que <br/>que esperas registrate.</p>
          <Link href="/login">
            <a>
              <ButtonSecundary>
                Registrate
              </ButtonSecundary>
            </a>
          </Link>
          {console.log(ButtonSecundary.value)}
      </div>
    </>
  )
}

export default StartTwo;