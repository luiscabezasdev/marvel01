import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import Link from 'next/link'

const Card = () => {

    const INITIAL_PAGE = 0;
    const [characters, setCharacters] = useState([]);
    const [searchHero, setSearchHero] = useState('');
    const [page, setPage] = useState(INITIAL_PAGE);
    
    
    // API
    useEffect(() => {

     const urlAPI = `https://gateway.marvel.com/v1/public/characters?limit=10&offset=${page}&apikey=4551d7d630115ec791819bd312472a13&hash=d96ca59f1543d8938d51b027d7f0723e&ts=20210531`;

     fetch(urlAPI)
        .then(response => response.json())
        .then(data => { 
        setCharacters(data.data.results)
                          })
     }, [page])

    
     // Paginación de los heroes  
    const handleLoadNext = () => {
        setPage(page + 10)
    }

    const handleLoadBefore = () => {
        setPage(page - 10)
    }

    // Busqueda de los herores
    const handleSearch = (event) => {
        event.preventDefault();
        setSearchHero(event.target.value)
    }

    const searchByName = (event) => {
        event.preventDefault();
        const urlNameAPI = `https://gateway.marvel.com/v1/public/characters?limit=10&nameStartsWith=${searchHero}&apikey=4551d7d630115ec791819bd312472a13&hash=d96ca59f1543d8938d51b027d7f0723e&ts=20210531`;   
        
        fetch(urlNameAPI)
        .then(response => response.json())
        .then(data => { 
        setCharacters(data.data.results)
        })

    }

     const filterHeros = characters.filter((hero) =>{
        return hero.name.toLowerCase().includes(searchHero.toLowerCase());
     })
     

    return (
        <div>
            <section className="search">
                <h1 className="search__title">Fin your favorite characters </h1>
                
                <div className="search__container">
                    <input className="input" type="text" onChange={handleSearch} placeholder='Search...'/>
                    <button className="button" type="button" onClick={searchByName}>Search</button>
                </div>

            </section>

            
            <section className="cards-button-title">
                    <h1 className="card__title">Characters</h1>

                    <div className="card-container-button">
                    {page === 0 ? (
                
                        <button className="card_button" onClick={()=>handleLoadNext()}>{'▶'}</button>
                    ):(
                        <div>
                            <button className="card_button" onClick={()=>handleLoadBefore()}>{'◀'}</button>
                            <button className="card_button" onClick={()=>handleLoadNext()}>{'▶'}</button>
                        </div>
                    )}
                    </div>
            </section>
            
            <section className="cards">
                    <div className="cards__container">
                    {
                    filterHeros.map(item => {       
                        const {id, name} = item;   
                      
                     return(   
                        <div key={id} className="cards-item">       
                             <img className="cards-item__img" src={`${item.thumbnail.path}.${item.thumbnail.extension}`} alt="Spiderman" />
                             <div className="cards-item__details">
                    
                              <p className="cards-item__details--title">{name}</p>
                      
                              <div className="cards-item__details--img">
                                <Image src="/images/Favoritos.png" height={30} width={30}/>
                                <Link href={`/character/${item.id}`}>
                                    Ver
                                </Link>
                             </div>
                         </div>                        
                        </div>
                     )
                     })
                    }
                                                  
                    
                    </div>
            </section>
        </div>
    )
}

export default Card
