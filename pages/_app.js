import { AuthProvider } from '../Context/AuthContext';
import '../styles/globals.scss';

export default function App({ Component, pageProps }) {
    return (
      <AuthProvider>
        <Component {...pageProps} />
      </AuthProvider>
    )
  }