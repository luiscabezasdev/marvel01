import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Layout from '../../pages/layout.js'


const character = () => {
    
    const router = useRouter()
    const { characterId } = router.query

    const [characterData, setCharacter] = useState([])
    const [comicsData, setComics] = useState([])

    useEffect(()=>{
        fetch(`https://gateway.marvel.com:443/v1/public/characters/${characterId}?apikey=4551d7d630115ec791819bd312472a13&hash=d96ca59f1543d8938d51b027d7f0723e&ts=20210531`)
        .then(result => result.json())
        .then(data => {
            console.log(data.data.results)
            setCharacter(data.data.results)
        })
    }, [])

    useEffect(() => {
        fetch(`https://gateway.marvel.com:443/v1/public/characters/${characterId}/comics?format=comic&apikey=4551d7d630115ec791819bd312472a13&hash=d96ca59f1543d8938d51b027d7f0723e&ts=20210531`)
        .then(result => result.json())
        .then(data => {
            console.log(data.data.results)
            setComics(data.data.results)
        })
    }, [])

    return (
        <Layout>
            {characterData.map((character) => (
                <div key={character.id}>
                    <section>
                        <h1 className="character__title">
                            {character.name}
                            {/* {Character.thumbnail.path} */}
                        </h1>

                    </section>
                    <section className="section">
                        <div className="favorites__container">
                            <div className="character">
                                <div className="character__container--img">
                                    <img
                                        className="character__img"
                                        src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
                                    />

                                </div>
                                <div className="character__info">
                                    <h3>Description:</h3>
                                    <p>{character.description === '' ? 'No description available' :  character.description}</p>
                                    <h3></h3>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="section">
                        <div className="favorites__container">
                            <div className="comics">
                                <h2 className="comic__title">Comics related</h2>
                                <ul className="comic__list">
                                    {comicsData.map(( comic )=>(
                                        <li className="comic__item" key={comic.id}>
                                            <img
                                                src={`${comic.thumbnail.path}.${comic.thumbnail.extension}`}
                                                className="comic__img"
                                            />
                                            <p className="comic_title">
                                                {comic.title}
                                            </p>
                                            <a>Add favorite</a>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </section>
                </div>

            ))}
                    
        </Layout>
    )
}

export default character
