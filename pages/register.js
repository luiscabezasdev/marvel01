import React from 'react'
import StartTwo from '../Components/StartTwo.js';
import Google from '../Components/Icons/Google';
import Facebook from '../Components/Icons/Facebook'
import ButtonSocial from '../Components/ButtonSocial';

const register = () => {
    return (
        <>
            <div className="contenedor">
                <StartTwo />
                <div className="content">
                    <h2>Sign in</h2>
                    <div className="content__btn">
                    <div className="content__btn">
                        <ButtonSocial>
                            <Google/>
                            Sign in with Google
                        </ButtonSocial>
                        <ButtonSocial>
                            <Facebook/>
                            Sign in with Facebook
                        </ButtonSocial>
                    </div>
                    </div>
                    <div className="content__line">
                        <div className="content__line--left"></div>
                        <div className="content__line--or">OR</div>
                        <div className="content__line--right"></div>
                    </div>
                    <form className="content__form">
                        <label className="email">
                            <span>Email</span>
                            <input type="email" placeholder="email"/>
                        </label>
                        <label className="password">
                            <span>Password</span>
                            <input type="password" placeholder="Password"/>
                        </label>
                        <div>
                            <ButtonSocial>
                                Sign in to your account
                            </ButtonSocial>
                        </div>
                    </form>

                </div>
            </div>
        </>
    )
}

export default register
